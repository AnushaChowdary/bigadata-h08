package com.spnotes.kafka.simple;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;
import java.util.Scanner;

/**
 * Created by sunilpatil on 12/28/15.
 */
public class Producer {
    private static Scanner in;
    private static int x;
    public static void main(String[] argv)throws Exception {
        if (argv.length != 1) {
            System.err.println("Please specify 1 parameters ");
            System.exit(-1);
        }
        String topicName = argv[0];
        in = new Scanner(System.in);
        // System.out.println("Enter message(type exit to quit)");

        //Configure the Producer
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.ByteArraySerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringSerializer");
        org.apache.kafka.clients.producer.Producer producer = new KafkaProducer(configProperties);
        
//        Random random = new Random();
            x=0;
            System.out.println("Enter your name: "); 
            String line = in.nextLine();
            System.out.println("On which month you born select from 1 to 12 ?");
            x = in.nextInt();
           
            String[] sentences = new String[] {
                line+" Loves to dress up and will take time to recover when hurt",
                line+" You are attractive, Intelligent and clever, Changing personality",
                line+" I think you are generous, honest and sympathetic",
                line+" you are very talkative, clam and cool",
                line+" you are strong willed and highly motivated",
                "Will have a fun time being with you "+line,
                line+" I think you love to make new friends",
                line+" you are charming and beautiful to everyone",
                "Hi "+line+" I think you loves to travel and explore",
                "Hey " +line+" You may be a day dreamer right!",
                line+" You are very emotional and an independent personality",
                line+" You are good looking personality compared to others and you active in games right!",
                
            };
//        String line = in.nextLine();
//        for(int i = 0; i < 20; i++) {
            // Pick a sentence at random
            //String s = sentences[random.nextInt(sentences.length)];
            // Send the sentence to the test topic

            String s = "-----------------\n"+sentences[x-1];
            ProducerRecord<String, String> rec = new ProducerRecord<String, String>(topicName, s);
            producer.send(rec);
            s = in.nextLine();
//        }
        in.close();
        producer.close();
    }
}
